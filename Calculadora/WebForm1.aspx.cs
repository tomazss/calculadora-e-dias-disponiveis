﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculadora
{
    public partial class WebForm1 : System.Web.UI.Page
    {
       

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            string operacao = ddlOperacao.SelectedValue;
            if (!txtValor1.Text.Equals("") && !txtValor2.Text.Equals(""))
            {
                if (operacao == "1")
                {
                    lblResultado.Text = $"O resuldato é:   <b> {(float.Parse(txtValor1.Text) + float.Parse(txtValor2.Text)).ToString()}</b>";

                }
                else if (operacao == "2")
                {
                    lblResultado.Text = $"O resuldato é:   <b> {(float.Parse(txtValor1.Text) - float.Parse(txtValor2.Text)).ToString()}</b>";

                }
                else if (operacao == "3")
                {
                    lblResultado.Text = $"O resuldato é:   <b>{(float.Parse(txtValor1.Text) / float.Parse(txtValor2.Text)).ToString()}</b>";

                }
                else if (operacao == "4")
                {
                    lblResultado.Text = $"O resuldato é:   <b> {(float.Parse(txtValor1.Text) * float.Parse(txtValor2.Text)).ToString()}</b>";

                }
                else
                {
                    lblResultado.Text = "Selecione alguma  operação";
                }
            }
            else
            {
                lblResultado.Text = "ERRO digite os dois valores";
            }
            
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            lblResultadoDiasDaSemana.Text = "Os dias da semana disponíveis são: <br/>";
            foreach(ListItem item in CheckBoxList1.Items)
            {
                if (item.Selected)
                {
                    lblResultadoDiasDaSemana.Text += $"<b>{item.Text}<b/> " ;
                }
            }
        }
    }
}