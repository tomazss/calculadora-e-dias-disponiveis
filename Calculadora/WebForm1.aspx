﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Calculadora.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Calculadora e Dias Disponiveis</title>
    <style>
        body{
            font-family: Arial,Arial, Helvetica, sans-serif;
            background-image:linear-gradient(to right,
            #3E8B9D, #1B4496, #1B1E6D, 
            #1E194F, #110E2D);
            


           
            
        }
        .container{
            display:flex;
            justify-content:center;
            width:100%;
            margin-top:3rem;
            flex-wrap:wrap;
        }
        .card{
            
            margin: 2rem;
            text-decoration: none;
            border: 1px solid white;
            padding: 1rem;
            background-color: rgba(0,0,0.4);
            border-radius: 15px;
            width: 300px;
            color: white;
            

        }
        
            
       
    </style>
</head>
<body>
   


        <form id="form1" runat="server" aria-orientation="vertical">
            <div class="container">
                 <div class="card" >  
            <asp:Label ID="lblCaculadora" runat="server" Text="Calculadora" Font-Bold="True" Font-Size="Larger" Width="100%"></asp:Label><br /><br />
                <asp:Label ID="valor1" runat="server" Text="Primeiro valor"></asp:Label><br />
        <asp:TextBox ID="txtValor1" runat="server" Width="80%"></asp:TextBox><br /><br />
        <asp:Label ID="valor2" runat="server" Text="Segundo valor"></asp:Label><br />
         <asp:TextBox ID="txtValor2" runat="server" Width="80%"></asp:TextBox><br /><br />
        <asp:DropDownList ID="ddlOperacao" runat="server" Width="80%">
            <asp:ListItem>Selecione a operação</asp:ListItem>
            <asp:ListItem Value="1">Soma</asp:ListItem>
            <asp:ListItem Value="2">Subtração</asp:ListItem>
            <asp:ListItem Value="3">Dividivisão</asp:ListItem>
            <asp:ListItem Value="4">Multiplicação</asp:ListItem>
        </asp:DropDownList><br /> <br />

        <asp:Button ID="btnCalcular" runat="server" Text="Calcular" Width="50%" OnClick="btnCalcular_Click" /><br /><br />
        <asp:Label ID="lblResultado" runat="server" ></asp:Label>

            </div>
            <div class="card">
                <asp:Label ID="lblDiasDisponiveis" runat="server" Text="Dias Disponíveis Para Trabalhar" Font-Bold="True" Font-Size="Larger" Width="100%"></asp:Label><br/><br />
                <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatColumns="2">
                    <asp:ListItem Value="1">Domingo</asp:ListItem>
                    <asp:ListItem Value="2">Segunda-feira</asp:ListItem>
                    <asp:ListItem Value="3">Terça-feira</asp:ListItem>
                    <asp:ListItem Value="4">Quarta-feira</asp:ListItem>
                    <asp:ListItem Value="5">Quinta-feira</asp:ListItem>
                    <asp:ListItem Value="6">Sexta-feira</asp:ListItem>
                    <asp:ListItem Value="7">Sábado</asp:ListItem>
                </asp:CheckBoxList><br />
                
               

        <asp:Button ID="btnEnviar" runat="server" Text="Enviar" Width="50%" OnClick="btnEnviar_Click"  /><br />
        <asp:Label ID="lblResultadoDiasDaSemana" runat="server" ></asp:Label>
            </div>
            </div>
              
            

        
    </form>

   

    
</body>
</html>
